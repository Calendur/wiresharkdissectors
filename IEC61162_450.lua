local nmea_protocol = Proto("IEC61162-450",  "IEC61162-450 Protocol")
local fields = nmea_protocol.fields
local experts = nmea_protocol.experts

fields.transmissionGroup    = ProtoField.string("nmea.transmissionGroup"      , "transmissionGroup")
fields.header               = ProtoField.string("nmea.header"                 , "header")
fields.tagBlock             = ProtoField.string("nmea.tag"                    , "tagBlock")
fields.tagSource            = ProtoField.string("nmea.tag.source"             , "source")
fields.tagDestination       = ProtoField.string("nmea.tag.destination"        , "destination")
fields.tagLineCount         = ProtoField.string("nmea.tag.lineCount"          , "lineCount")
fields.tagGrouping          = ProtoField.string("nmea.tag.grouping"           , "grouping")
fields.tagGroupingSeqIndex  = ProtoField.string("nmea.tag.groupingSeqIndex"   , "grouping sequence index")
fields.tagGroupingSeqCount  = ProtoField.string("nmea.tag.groupingSeqCount"   , "grouping sequence count")
fields.tagGroupingCode      = ProtoField.string("nmea.tag.groupingCode"       , "group code")
fields.tagChecksum          = ProtoField.string("nmea.tag.checksum"           , "checksum")
experts.tagChecksumInvalid  = ProtoExpert.new(  "nmea.tag.checksumInvalid"    , "Tag block checksum is invalid", expert.group.MALFORMED, expert.severity.ERROR)
fields.nmeaMessage          = ProtoField.string("nmea.message"                , "nmeaMessage")
fields.nmeaHeader           = ProtoField.string("nmea.message.header"         , "header")
fields.nmeaChecksum         = ProtoField.string("nmea.message.checksum"       , "checksum")
experts.nmeaChecksumInvalid = ProtoExpert.new(  "nmea.message.checksumInvalid", "NMEA checksum is invalid", expert.group.MALFORMED, expert.severity.ERROR)


local transmissionGroupPorts = {
  [60001] = "MISC",
  [60002] = "TGTD",
  [60003] = "SATD",
  [60004] = "NAVD",
  [60005] = "VDRD",
  [60006] = "RCOM",
  [60007] = "TIME",
  [60008] = "PROP",
  [60009] = "USR1",
  [60010] = "USR2",
  [60011] = "USR3",
  [60012] = "USR4",
  [60013] = "USR5",
  [60014] = "USR6",
  [60015] = "USR7",
  [60016] = "USR8",
}
function getTransmissionGroupName(port)
  if transmissionGroupPorts[port] then
    return transmissionGroupPorts[port];
  else
    return ""
  end
end

function parseGroupingTag(tree, buffer)
  local firstSep  = string.find(buffer:string(), '-')
  local secondSep = string.find(buffer:string(), '-', firstSep + 1)
  if (firstSep and secondSep)
  then
    tree:add(fields.tagGroupingSeqIndex, buffer(0, firstSep - 1))
    tree:add(fields.tagGroupingSeqCount, buffer(firstSep, secondSep - firstSep - 1))
    tree:add(fields.tagGroupingCode, buffer(secondSep))
  end
end


local knownTagBlockIdentifiers = {
  ['s'] = fields.tagSource,
  ['d'] = fields.tagDestination,
  ['g'] = fields.tagGrouping,
  ['n'] = fields.tagLineCount,
}

function addTagBlockField(tree, buffer)
  local colon = string.find(buffer:string(), ':')
  if (colon ~= 2) then
    return
  end
  local identifier = buffer(0, 1):string()
  
  if (knownTagBlockIdentifiers[identifier]) then
    local subtree = tree:add(knownTagBlockIdentifiers[identifier], buffer(colon))
    
    -- special handling for grouping header
    if (identifier == 'g') then
      parseGroupingTag(subtree, buffer(colon))
    end
  end
end


local numberToHexMap = {
  [ 0] = "0",
  [ 1] = "1",
  [ 2] = "2",
  [ 3] = "3",
  [ 4] = "4",
  [ 5] = "5",
  [ 6] = "6",
  [ 7] = "7",
  [ 8] = "8",
  [ 9] = "9",
  [10] = "A",
  [11] = "B",
  [12] = "C",
  [13] = "D",
  [14] = "E",
  [15] = "F",
}

function string.tohex(hex)
  local upper = numberToHexMap[math.floor(hex / 16)]
  local lower = numberToHexMap[hex % 16]
  if (upper and lower)
  then
    return upper .. lower
  end
  return ""
end
function string.fromhex(str)
  return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
  end))
end

function parseChecksum(tree, checksumField, expertInfo, message, checksumBuf)
  local checkSumField = tree:add(checksumField, checksumBuf)
  
  local calcChecksum = 0
  for i=0,message:len()-1 do
    calcChecksum = bit32.bxor(calcChecksum, message(i, 1):uint())
  end
  local msgChecksum = string.byte(string.fromhex(checksumBuf:string()))
  if (msgChecksum ~= calcChecksum) then
    tree:add_proto_expert_info(expertInfo)
    checkSumField:append_text(" (Checksum should be " .. string.tohex(calcChecksum) .. ")")
  end
end

-- parse a single tag block and add it to the tree
-- it will look for fields inside the tag block and adds them to the tree as well
function parseNMEATagBlock(tree, buffer)
  local subtree = tree:add(fields.tagBlock, buffer)
  local lastComma = 1
  -- allow at most 15 elements inside the tag block (to avoid endless loops)
  for i=1,15 do
    local comma = string.find(buffer:string(), ',', lastComma + 1)
    if (comma) then
      addTagBlockField(subtree, buffer(lastComma, comma - lastComma - 1))
      lastComma = comma
    else
      break
    end
  end
  local checksumDelim = string.find(buffer:string(), '*')
  addTagBlockField(subtree, buffer(lastComma, checksumDelim - lastComma - 1))
  parseChecksum(subtree, fields.tagChecksum, experts.tagChecksumInvalid, buffer(1, checksumDelim - 2), buffer(checksumDelim, 2))
end

-- parse a single NMEA message and add it to the tree
-- returns the parsed nmeaHeader as string
-- if nothing could be parsed, it returns nil
function parseNMEAMessage(tree, buffer)
  local subtree = tree:add(fields.nmeaMessage, buffer)
  if (buffer(0, 1):string() == '$' or buffer(0, 1):string() == '!')
  then
    local nmeaComma  = string.find(buffer:string(), ',')
    local checksumDelim = string.find(buffer:string(), '*')
    if (nmeaComma == nil) then
      nmeaComma = checksumDelim
    end
    subtree:add(fields.nmeaHeader,   buffer(1, nmeaComma - 2))
    parseChecksum(subtree, fields.nmeaChecksum, experts.nmeaChecksumInvalid, buffer(1, checksumDelim - 2), buffer(checksumDelim, 2))
    
    return buffer(1, nmeaComma - 2):string()
  end
  return nil
end

-- parse the nmea message buffer
-- it looks for tag blocks and NMEA messages
function splitMessage(pinfo, subtree, buffer)
  -- find multiple tag blocks, until the last '\' sign
  local lastTagEnd = 1
  local tagEnd = string.find(buffer:string(), '\\', 2)
  while (tagEnd)
  do
    parseNMEATagBlock(subtree, buffer(lastTagEnd - 1, tagEnd - lastTagEnd))
    
    lastTagEnd = tagEnd
    tagEnd = string.find(buffer:string(), '\\', lastTagEnd + 1)
  end
  
  -- find multiple NMEA messages (separated by a '\r\n' sequence)
  local nmeaHeaders = ""
  local lastMsgEnd = lastTagEnd - 1
  local msgEnd = string.find(buffer:string(), '\r\n', lastMsgEnd + 1)
  while (msgEnd)
  do
    local nmeaHeader = parseNMEAMessage(subtree, buffer( lastMsgEnd + 1, msgEnd - lastMsgEnd))
    if (nmeaHeader)
    then      
      if (nmeaHeaders ~= "") then
        nmeaHeaders = nmeaHeaders .. ", "
      end
      nmeaHeaders = nmeaHeaders .. nmeaHeader
    end
    
    lastMsgEnd = msgEnd
    msgEnd = string.find(buffer:string(), '\r\n', lastMsgEnd + 1)
  end
  
  pinfo.cols.info = nmeaHeaders
end

-- the main dissector function (called directly from wireshark)
function nmea_protocol.dissector(buffer, pinfo, tree)
  length = buffer:len()
  if length == 0 then return end

  pinfo.cols.protocol = nmea_protocol.name .. " - " .. getTransmissionGroupName(pinfo.dst_port)

  local subtree = tree:add(nmea_protocol, buffer(), "IEC61162-450 Data")
  
  subtree:add(fields.header,  buffer( 0,  6))
  splitMessage(pinfo, subtree, buffer( 6,  buffer:len() - 6))
end

local udp_port = DissectorTable.get("udp.port")
for port=60001,60016 do
  udp_port:add(port, nmea_protocol)
end

