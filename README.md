# WiresharkDissectors

## Purpose

This repository will contain different dissectors for wireshark which could be helpful to parse some not yet supported protocols.

The following dissectors are currently inside this repository:
- IEC 61162-450 Edition 1


## Installation

To install those dissectors simply copy the lua files into your wireshark installation folder.
E.g. copy the lua to C:\Program Files\Wireshark\plugins\2.6

## IEC 61162-450 Edition 1

This dissector currently supports IEC 61162-450 Edition 1.
It registers on ports 60001 to 60016.
It will show the transmission group inside the protocol field of wireshark.
It will show the contained NMEA sentences inside the info field of wireshark.
The dissector parses the tag block and the NMEA messages. The dissector checks the tag block and NMEA message checksums.

![Example Dissector Output](https://gitlab.com/Calendur/wiresharkdissectors/tree/master/IEC61162-450_Data.png "Example Dissector Output")
